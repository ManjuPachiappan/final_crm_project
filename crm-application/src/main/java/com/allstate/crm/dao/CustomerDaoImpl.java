package com.allstate.crm.dao;

import com.allstate.crm.entities.Customer;
import com.allstate.crm.entities.Interaction;
import com.allstate.crm.exceptions.CustomerException;
import com.mongodb.client.result.DeleteResult;
import com.mongodb.client.result.UpdateResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CustomerDaoImpl implements CustomerDao {
    @Autowired
    MongoTemplate mongoTemplate;

    @Override
    public List<Customer> findCustomerByName(String name) {
        Query query = new Query();
        query.addCriteria(Criteria.where("name").is(name));
        return mongoTemplate.find(query, Customer.class);
    }

    @Override
    public Customer findCustomerById(int customerId) {
        Query query = new Query();
        query.addCriteria(Criteria.where("customerId").is(customerId));
        return mongoTemplate.findOne(query, Customer.class);
    }

    @Override
    public List<Customer> findAllCustomer() {
       return mongoTemplate.findAll(Customer.class);
    }

    @Override
    public int addCustomer(Customer customer) throws CustomerException {
        Customer customerAdded= mongoTemplate.insert(customer);
        if (customerAdded !=null)
        {return 1;}
        else
        {return 0;}
    }

    @Override
    public int deleteCustomer(int customerId) {
        Query query = new Query();
        query.addCriteria(Criteria.where("customerId").is(customerId));
        DeleteResult deleteResult= mongoTemplate.remove(query, Customer.class);
        if(deleteResult!=null) {
            return (int) deleteResult.getDeletedCount();
        }else{
            return 0;
        }
    }

    @Override
    public int updateCustomer(Customer customer) {
        Query query = new Query();
        query.addCriteria(Criteria.where("customerId").is(customer.getCustomerId()));
        Update update = new Update();
        update.set("name", customer.getName());
        update.set("age", customer.getAge());
        update.set("address", customer.getAddress());
        update.set("email", customer.getEmail());
        update.set("phoneNumber", customer.getPhoneNumber());

        UpdateResult updateResult=mongoTemplate.updateFirst(query, update, Customer.class);
        if(updateResult!=null){
            return (int) updateResult.getModifiedCount();
        }else {
            return 0;
        }

    }
}
