package com.allstate.crm.rest;


import com.allstate.crm.entities.Customer;
import com.allstate.crm.exceptions.CustomerException;
import com.allstate.crm.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.logging.Logger;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("crm/customer")
public class CustomerController {
    @Autowired
    CustomerService customerService;
    Logger logger = Logger.getLogger(CustomerController.class.getName());

    @RequestMapping(value = "/add" ,method = RequestMethod.POST)
    public int addCustomer(@RequestBody Customer customer) {

        return customerService.addCustomer(customer);
    }

    @RequestMapping(value = "/update" ,method = RequestMethod.PUT)
    public int updateCustomer(@RequestBody Customer customer) {

        return customerService.updateCustomer(customer);
    }

    @RequestMapping(value = "/delete/{customerId}" ,method = RequestMethod.POST)
    public int deleteCustomer(@RequestBody@PathVariable("customerId") int customerId) {
        return customerService.deleteCustomer(customerId);
    }
    @RequestMapping(value = "/find/{customerId}", method = RequestMethod.GET)
    public ResponseEntity<Customer> findCustomerById(@PathVariable("customerId") int customerId) {
        Customer customer = customerService.findCustomerById(customerId);

        if (customer != null) {
            return new ResponseEntity<Customer>(customer, HttpStatus.OK);

        } else {
            return new ResponseEntity<Customer>(HttpStatus.NOT_FOUND);
        }
    }
    @RequestMapping(value = "/findall", method = RequestMethod.GET)
    public ResponseEntity<List> findAll()  {
        List<Customer> customerList = customerService.findAllCustomer();

        if (customerList.size() == 0) {
            return new ResponseEntity<List>(HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<List>(customerList, HttpStatus.OK);
        }
    }
    @RequestMapping(value = "/findbyname/{name}", method = RequestMethod.GET)
    public ResponseEntity<List> findCustomerByName(@PathVariable("name") String name) {
        List<Customer> customerList = customerService.findCustomerByName(name);
        return new ResponseEntity<List>(customerList, HttpStatus.OK);

       /* if (customerList.size() == 0) {
            return new ResponseEntity<List>(HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<List>(customerList, HttpStatus.OK);
        }*/

    }
    @ExceptionHandler({CustomerException.class})
    public ResponseEntity<String> handleException(CustomerException e) {
        logger.warning(String.format("Customer Exception raised: %s", e.getMessage()));
        return ResponseEntity.badRequest().body(e.getMessage());
    }

}
