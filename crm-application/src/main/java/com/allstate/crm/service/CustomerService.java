package com.allstate.crm.service;

import com.allstate.crm.entities.Customer;
import com.allstate.crm.exceptions.CustomerException;

import java.util.List;

public interface CustomerService {
    List<Customer> findCustomerByName(String name);
    Customer findCustomerById(int customerId);
    List<Customer> findAllCustomer();
    int addCustomer(Customer customer) throws CustomerException;
    int deleteCustomer(int customerId);
    int updateCustomer(Customer customer) throws CustomerException;
}
