package com.allstate.crm.entities;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Document
public class Interaction {

    @Id
    private int interactionId;
    private int customerId;
    private String interactionDetails;
    private Date interactionDateAndTime;
    private String interactionType;

    public Interaction(int interactionId, int customerId, String interactionDetails, Date interactionDateAndTime, String interactionType) {
        this.interactionId = interactionId;
        this.customerId = customerId;
        this.interactionDetails = interactionDetails;
        this.interactionDateAndTime = interactionDateAndTime;
        this.interactionType = interactionType;
    }

    public Interaction() {
    }

    public int getInteractionId() {
        return interactionId;
    }

    public void setInteractionId(int interactionId) {
        this.interactionId = interactionId;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public String getInteractionDetails() {
        return interactionDetails;
    }

    public void setInteractionDetails(String interactionDetails) {
        this.interactionDetails = interactionDetails;
    }

    public Date getInteractionDateAndTime() {
        return interactionDateAndTime;
    }

    public void setInteractionDateAndTime(Date interactionDateAndTime) {
        this.interactionDateAndTime = interactionDateAndTime;
    }

    public String getInteractionType() {
        return interactionType;
    }

    public void setInteractionType(String interactionType) {
        this.interactionType = interactionType;
    }

    public String toString() {
        return "Interaction{Interaction Id: " + this.interactionId + " Customer Id: " + this.customerId + " Interactions Type: " + this.interactionType + "Interactions Details " + this.interactionDetails + "Interactions DateAndTime: " + this.interactionDateAndTime + "}";
    }


}
