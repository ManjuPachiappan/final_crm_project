import axios from "axios";

// for our Action Creators
export const fetchCustomersBegin = () => {
  return {
    type: "FETCH_CUSTOMERS_BEGIN",
  };
};

export const fetchCustomersSuccess = (customers) => {
  return {
    type: "FETCH_CUSTOMERS_SUCCESS",
    payload: customers,
  };
};

export const fetchCustomersFailure = (err) => {
  return {
    type: "FETCH_CUSTOMERS_FAILURE",
    payload: { message: "Failed to fetch customers.. please try again later" },
  };
};

export const getApiStatusBegin = () => {
  return {
    type: "GET_APISTAUS_BEGIN",
  };
};

export const getApiStatusSuccess = (status) => {
  return {
    type: "GET_APISTATUS_SUCCESS",
    payload: status,
  };
};

export const getApiStatusFailure = (err) => {
  return {
    type: "GET_APISTATUS_FAILURE",
    payload: { message: "Failed to get staus.. please try again later" },
  };
};

// to be call by the components
export const fetchCustomers = () => {
  // returns the thunk function
  return (dispatch, getState) => {
    dispatch(fetchCustomersBegin());
    console.log("state after fetchCustomersBegin", getState());
    axios.get("http://localhost:8080/crm/customer/findall").then(
      (res) => {
        setTimeout(() => {
          // // Need to remove this static json when API is ready
          // res.data = [
          //   {
          //     customerId: 1,
          //     name: "Darshan",
          //     age: 22,
          //     email: "darsh111@gmail.com",
          //     address: "101 strt walk",
          //     phone: 9739195196,
          //   },
          //   {
          //     customerId: 2,
          //     name: "Ram",
          //     age: 25,
          //     email: "ram111@gmail.com",
          //     address: "106 strt walk",
          //     phone: 9739195198,
          //   },
          //   {
          //     customerId: 3,
          //     name: "Shreya",
          //     age: 28,
          //     email: "sssss111@gmail.com",
          //     address: "109 strt walk",
          //     phone: 9739195609,
          //   },
          // ];
          // remove json till here
          dispatch(fetchCustomersSuccess(res.data));
          console.log("state after fetchCustomersSuccess", getState());
        }, 3000);
      },
      (err) => {
        // dispatch FETCH_CUSTOMERS_FAILURE
        dispatch(fetchCustomersFailure(err));
        console.log("state after fetchCustomersFailure", getState());
      }
    );
  };
};

export const addCustomerBegin = () => {
  return {
    type: "ADD_CUSTOMER_BEGIN",
  };
};

export const addCustomerSuccess = () => {
  return {
    type: "ADD_CUSTOMER_SUCCESS",
  };
};

export const addCustomerFailure = (err) => {
  return {
    type: "ADD_CUSTOMER_FAILURE",
    payload: { message: "Failed to add new customer.. please try again later" },
  };
};

export const updateCustomerBegin = () => {
  return {
    type: "UPDATE_CUSTOMER_BEGIN",
  };
};

export const updateCustomerSuccess = () => {
  return {
    type: "UPDATE_CUSTOMER_SUCCESS",
  };
};

export const updateCustomerFailure = (err) => {
  return {
    type: "UPDATE_CUSTOMER_FAILURE",
    payload: { message: "Failed to update customer.. please try again later" },
  };
};

function delay(t, v) {
  return new Promise(function (resolve) {
    setTimeout(resolve.bind(null, v), t);
  });
}

export const addCustomer = (customer) => {
  // returns our async thunk function
  return (dispatch, getState) => {
    return axios.post("http://localhost:8080/crm/customer/add", customer).then(
      () => {
        console.log("customer created!");
        // this is where we can dispatch ADD_CUSTOMER_SUCCESS
        dispatch(addCustomerSuccess());
      },
      (err) => {
        dispatch(addCustomerFailure(err));
        console.log("state after addCustomerFailure", getState());
      }
    );
  };
};

export const getApiStatus = () => {
  // returns the thunk function
  return (dispatch, getState) => {
    dispatch(getApiStatusBegin());
    console.log("state after getApiStatusBegin", getState());
    axios.get("http://localhost:8080/crm/interactions/status").then(
      (res) => {
        setTimeout(() => {
          dispatch(getApiStatusSuccess(res.data));
          console.log(res.data);
          console.log("state after getApiStatusSuccess", getState());
        }, 3000);
      },
      (err) => {
        // dispatch GET_APISTATUS_FAILURE
        dispatch(getApiStatusFailure(err));
        console.log("state after getApiStatusFailure", getState());
      }
    );
  };
};

export const updateCustomer = (customer) => {
  // returns our async thunk function
  return (dispatch, getState) => {
    dispatch(updateCustomerBegin());
    return axios
      .put("http://localhost:8080/crm/customer/update", customer)
      .then(
        () => {
          console.log("customer updated!");
          // this is where we can dispatch UPDATE_CUSTOMER_SUCCESS
          dispatch(updateCustomerSuccess());
        },
        (err) => {
          dispatch(updateCustomerFailure(err));
          console.log("state after updateCustomerFailure", getState());
        }
      );
  };
};
