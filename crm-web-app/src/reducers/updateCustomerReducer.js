const initialState = { entities: [] };

export default (state = initialState, action) => {
  console.log(`received ${action.type} dispatch in updateCustomerReducer`);
  switch (action.type) {
    case "UPDATE_CUSTOMER_BEGIN":
      return { ...state, loading: true, error: null };
    case "UPDATE_CUSTOMER_SUCCESS":
      return { ...state, loading: false };
    case "UPDATE_CUSTOMER_FAILURE":
      return { ...state, loading: false, error: action.payload };
    default:
      return state;
  }
};
