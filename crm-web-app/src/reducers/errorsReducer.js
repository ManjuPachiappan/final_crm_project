const errorsReducer = (state = [], action) => {
  console.log(`received ${action.type} dispatch in errorsReducer`);
  switch (action.type) {
    case "FETCH_CUSTOMERS_FAILURE":
    case "GET_APISTATUS_FAILURE":
    case "ADD_CUSTOMER_FAILURE":
      return [...state, action.payload];
    default:
      return state;
  }
};

export default errorsReducer;
