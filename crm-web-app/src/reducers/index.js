import { combineReducers } from "redux";
import customersReducer from "./customersReducer";
import errorsReducer from "./errorsReducer";
import apiStatusReducer from "./apiStatusReducer";
import updateCustomerReducer from "./updateCustomerReducer";

const rootReducer = combineReducers({
  customers: customersReducer,
  apiStatus: apiStatusReducer,
  updateCustomer: updateCustomerReducer,
  errors: errorsReducer,
});

export default rootReducer;
