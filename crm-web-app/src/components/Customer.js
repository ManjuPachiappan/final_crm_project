import React, { useState } from "react";
import CustomerName from "./CustomerName";
import CustomerDetails from "./CustomerDetails";
import ShowHideButton from "./ShowHideButton";
import UpdateCustomerButton from "./UpdateCustomerButton";

const Customer = ({ customerId, name, age, email, address, phoneNumber }) => {
  const [visible, setVisibity] = useState(true);
  // const [fetchCustomer, setFetchCustomer] = useState(false);

  return (
    <div className="col-md-4">
      <div className="card mb-4 box-shadow" style={{ width: "18rem" }}>
        <div className="card-body">
          <CustomerName name={name} />
          <CustomerDetails
            customerId={customerId}
            age={age}
            email={email}
            address={address}
            phoneNumber={phoneNumber}
            visible={visible}
          />
          <ShowHideButton toggle={setVisibity} visible={visible} />
          <UpdateCustomerButton
            customerId={customerId}
            name={name}
            age={age}
            email={email}
            address={address}
            phoneNumber={phoneNumber}
          />
        </div>
      </div>
    </div>
  );
};

export default Customer;
