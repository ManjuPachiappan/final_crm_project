import React from "react";

const Banner = () => (
  <section className="jumbotron text-center">
    <div className="container">
      <h1 className="jumbotron-heading">Customer Relationship Management</h1>
      <p className="lead text-muted">CRM.</p>
    </div>
  </section>
);

export default Banner;
